/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
  app_name : ['Drywall'],
  license_key : '5277c6e4ff9a75e27c5aa00d26e4f2c3aba00c60',
  logging : { level : 'info'}
};
